#include <windows.h> 
#include <string.h> 
#include <stdio.h> 
#include <fstream> 
#include <list> 
#include <iostream>
using namespace std;

#include "resource.h" 

#define SERVER_PORT 7799 

#define SERVER_NUM 5
#define Midline 256

#define F_CONNECTING 0
#define F_READING 1
#define F_WRITING 2
#define F_DONE 3 

//read buffer size
#define SIZEOF_BUFF 8000

#define WM_SOCKET_NOTIFY (WM_USER + 1) 
#define CGI_SERVER_CONNECTION (SERVER_NUM + 1) 

BOOL CALLBACK MainDlgProc(HWND, UINT, WPARAM, LPARAM);

int EditPrintf(HWND, TCHAR *, ...);
void parse_INFO(char * query_string);
int readline(FILE *fd, char *ptr, int maxlen);
void connect2Server(HWND hwndEdit, HWND hwnd);
void print_table_row(SOCKET ssock);
void end_html(SOCKET ssock);
void begin_html(SOCKET ssock);
void transfer_data(FILE *fd, SOCKET ssock);
void sendInst(int i,SOCKET ssock, char buffer[SIZEOF_BUFF],int datalen);
//================================================================= 
//	Global Variables 
//================================================================= 
list< SOCKET> Socks;


struct sockaddr_in address[SERVER_NUM];
struct hostent * host;

int conn = 0;
class Myser {
//server ip port filename

public:
	char host[Midline];
	char charport[Midline];
	char filename[Midline];

	int port;	
	int sock_fd;
	int NeedWrite;
	int status;	
	int id;

	FILE *fd;
};
Myser sc[SERVER_NUM];



HINSTANCE hInst;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	return DialogBox(hInstance, MAKEINTRESOURCE(ID_MAIN), NULL, MainDlgProc);
}


BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	WSADATA wsaData;

	static HWND hwndEdit;
	static SOCKET msock, ssock;
	static struct sockaddr_in sa;

	int err;

	switch (Message)
	{
	case WM_INITDIALOG:
		hwndEdit = GetDlgItem(hwnd, IDC_RESULT);
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case ID_LISTEN:

			WSAStartup(MAKEWORD(2, 0), &wsaData);

			//create master socket 
			msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

			if (msock == INVALID_SOCKET) {
				EditPrintf(hwndEdit, TEXT("=== Error: create socket error ===\r\n"));
				WSACleanup();
				return TRUE;
			}

			err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE);

			if (err == SOCKET_ERROR) {
				EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
				closesocket(msock);
				WSACleanup();
				return TRUE;
			}

			//fill the address info about server 
			sa.sin_family = AF_INET;
			sa.sin_port = htons(SERVER_PORT);
			sa.sin_addr.s_addr = INADDR_ANY;

			//bind socket 
			err = bind(msock, (LPSOCKADDR)&sa, sizeof(struct sockaddr));

			if (err == SOCKET_ERROR) {
				EditPrintf(hwndEdit, TEXT("=== Error: binding error ===\r\n"));
				WSACleanup();
				return FALSE;
			}

			err = listen(msock, 2);

			if (err == SOCKET_ERROR) {
				EditPrintf(hwndEdit, TEXT("=== Error: listen error ===\r\n"));
				WSACleanup();
				return FALSE;
			}
			else {
				EditPrintf(hwndEdit, TEXT("=== Server START ===\r\n"));
			}

			break;
		case ID_EXIT:
			EndDialog(hwnd, 0);
			break;
		};
		break;

	case WM_CLOSE:
		EndDialog(hwnd, 0);
		break;
	case WM_SOCKET_NOTIFY:
		switch (WSAGETSELECTEVENT(lParam))
		{
			case FD_ACCEPT:
				ssock = accept(msock, NULL, NULL);
				Socks.push_back(ssock);
				err = WSAAsyncSelect(ssock, hwnd, WM_SOCKET_NOTIFY, FD_READ | FD_WRITE);
				if (err == SOCKET_ERROR) {
					EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
					closesocket(ssock);
					WSACleanup();
				}
				EditPrintf(hwndEdit, TEXT("=== Accept one new client(%d), List size:%d ===\r\n"), ssock, Socks.size());
				break;
			case FD_READ:
				//Write your code for read event here. 
				char buffer[1024];
				char *filename;
				int datalen;
				datalen = recv(ssock, buffer, sizeof(buffer)-1, 0);
				if (datalen <= 0)
					printf("error !! \n");
				else
				{
					//cout << buffer << endl;
					strtok(buffer, "/");
					filename = strtok(NULL, " ");
					char *have_cgi_file; 
					have_cgi_file= strstr(filename,".cgi");
					send(ssock, "HTTP/1.1 200 OK", strlen("HTTP/1.1 200 OK"), 0);
					send(ssock, "Content-Type: text/html\n\n", strlen("Content-Type: text/html\n\n"), 0);
						
					if (have_cgi_file)
					{
						char *qs;
						strtok(filename, "?");
						qs = strtok(NULL, " ");
						parse_INFO(qs);
						conn = 0;
						begin_html(ssock);
						print_table_row(ssock);
						end_html(ssock);
						connect2Server(hwndEdit, hwnd);
					}
					else
					{
						FILE *fd;
						fd = fopen(filename, "r");
						if (fd == NULL)
						{

							send(ssock, "<html><body>Not Found</body></html>", strlen("<html><body>Not Found</body></html>"), 0);
							closesocket(ssock);

						}
						else
						{
							transfer_data(fd,ssock);
							closesocket(ssock);
							Socks.pop_back();
						}
					}

				}
				break;
			default:
				break;
		};
		break;
	case CGI_SERVER_CONNECTION:
		switch (WSAGETSELECTEVENT(lParam))
		{
			case FD_READ:
				int i;
				for (i = 0; i < SERVER_NUM; i++)
				{
					if (strlen(sc[i].host) > 0)
					{
						if (sc[i].status == F_READING)
						{
							EditPrintf(hwndEdit, TEXT("reading\n"));
							char read_buf[SIZEOF_BUFF];
							char sendbuff[SIZEOF_BUFF];
							int datalen;
							datalen = recv(sc[i].sock_fd, read_buf, SIZEOF_BUFF - 1, 0);

							if (datalen <= 0){
								EditPrintf(hwndEdit, TEXT("recieve from server error \n"));
							}
							else
							{
								/*
								<script>document.all['m1'].innerHTML += "<br>";</script>
								<script>document.all['m1'].innerHTML += "Test<br>";</script>
								<script>document.all['m1'].innerHTML += "This is a test program<br>";</script>
								<script>document.all['m%d'].innerHTML += \"%s \";</script>
								*/
								read_buf[datalen] = 0;
								sprintf(sendbuff, "<script>document.all['m%d'].innerHTML+=\"", i);
								send(ssock, sendbuff, strlen(sendbuff), 0);
								int j;
								for (j = 0; j < datalen - 1; j++)
								{
									if (read_buf[j] == '\n')
										send(ssock, "<br>", strlen("<br>"), 0);
									else
										send(ssock, &read_buf[j], 1, 0);
								}
								if (!strstr(read_buf, "% "))
									send(ssock, "<br>\";</script>\n", strlen("<br>\";</script>\n"), 0);
								
								if (strstr(read_buf, "% "))
								{
									send(ssock, "\";</script>\n", strlen("\";</script>\n"), 0);
									//change to WRITINR state
									sc[i].status = F_WRITING;
									if (sc[i].status == F_WRITING)
									{
										int datalen;
										char msg_buf[SIZEOF_BUFF];

										datalen = readline(sc[i].fd, msg_buf, SIZEOF_BUFF - 1);

										msg_buf[datalen - 1] = 13;
										msg_buf[datalen] = 10;
										msg_buf[datalen + 1] = '\0';
										
										sendInst(i,ssock, msg_buf,datalen);
										if (strstr(msg_buf, "exit"))
										{
											send(sc[i].sock_fd, msg_buf, strlen(msg_buf), 0);
											sc[i].status = F_DONE;

											closesocket(sc[i].sock_fd);
											fclose(sc[i].fd);
											if ((--conn) == 0)
												closesocket(ssock);
										}
										else
										{
											send(sc[i].sock_fd, msg_buf, strlen(msg_buf), 0);
											if (strstr(msg_buf, "\n"))
												sc[i].status = F_READING;
										}
									}
								}
							}
						}

					}
				}
			default:
				break;
		}
	default:
		return FALSE;


	};

	return TRUE;
}
void sendInst(int user,SOCKET ssock, char buffer[SIZEOF_BUFF],int datalen){
	char sendbuff[SIZEOF_BUFF];
	sprintf(sendbuff, "<script>document.all['m%d'].innerHTML+=\"<b>", user);
	send(ssock, sendbuff, strlen(sendbuff), 0);
	int j;
	for (j = 0; j < datalen - 1; j++)
	{
		if (buffer[j] == '\n')
			send(ssock, "<br>", strlen("<br>"), 0);
		else if (buffer[j] == 13 || buffer[j] == '\t' || buffer[j] == '\r');
		else send(ssock, &buffer[j], 1, 0);
	}
	send(ssock, "<br></b>\";</script>\n", strlen("<br></b>\";</script>\n"), 0);
}
void transfer_data(FILE *fd , SOCKET ssock){
	long lSize;
	char * buffer;
	size_t result;

	fseek(fd, 0, SEEK_END);
	lSize = ftell(fd);
	rewind(fd);
	buffer = (char*)malloc(sizeof(char)*lSize);
	result = fread(buffer, 1, lSize, fd);
	//EditPrintf(hwndEdit, TEXT("%s"), buffer); 
	send(ssock, buffer, result, 0);
	memset(buffer, 0, sizeof(buffer));
}
void parse_INFO(char * query_string)
{
	char ip[Midline], port[Midline], file[Midline];
	char * token;
	int i;
	for (i = 0; i < SERVER_NUM; i++)
	{
		if (i == 0)
			token = strtok(query_string, "&");
		else
			token = strtok(NULL, "&");
		sprintf(ip, "h%d=", i + 1);
		if (!strncmp(token, ip, 3) && strlen(token) > 3)
		{
			strncpy(sc[i].host, (token + 3), strlen(token + 3));
		}
		sprintf(port, "p%d=", i + 1);
		token = strtok(NULL, "&");
		if (!strncmp(token, port, 3) && strlen(token) > 3)
		{
			strncpy(sc[i].charport, (token + 3), strlen(token + 3));
			sc[i].port = atoi(sc[i].charport);
		}
		sprintf(file, "f%d=", i + 1);
		token = strtok(NULL, "&");
		if (!strncmp(token, file, 3) && strlen(token) > 3)
		{
			strncpy(sc[i].filename, (token + 3), strlen(token + 3));
		}
		if (strlen(sc[i].host))
		{
			sc[i].fd = fopen(sc[i].filename, "r");
		}
	}
	
}
void connect2Server(HWND hwndEdit, HWND hwnd)
{
	int err;

	for (int i = 0; i < SERVER_NUM; i++){

		if (strlen(sc[i].host) > 0  ){

			EditPrintf(hwndEdit, TEXT("client%d connect server \n"), i);
			if ((sc[i].sock_fd = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
				EditPrintf(hwndEdit, TEXT("client%d connect server error\n"), i);

			if ((host = gethostbyname(sc[i].host)) == NULL)
				EditPrintf(hwndEdit, TEXT("=== Error: gethostbyname error (server)==\r\n"));

			address[i].sin_family = AF_INET;
			address[i].sin_port = htons(sc[i].port);
			address[i].sin_addr.s_addr = *((unsigned long *)host->h_addr);

			err = WSAAsyncSelect(sc[i].sock_fd, hwnd, CGI_SERVER_CONNECTION, FD_CONNECT | FD_CLOSE | FD_READ | FD_WRITE);
			if (err == SOCKET_ERROR){

				EditPrintf(hwndEdit, TEXT("=== Error: select error (server)==\r\n"));
				closesocket(sc[i].sock_fd);
				WSACleanup();
			}
			if ((err = connect(sc[i].sock_fd, (struct sockaddr *) &address[i], sizeof(address[i]))) <= 0 && WSAGetLastError() != WSAEWOULDBLOCK){
				
				EditPrintf(hwndEdit, TEXT("=== Error: connect error (server %d)==\r\n"), i);
			}
			sc[i].status = F_READING;
		}
	}
}
void print_table_row(SOCKET ssock){
	int i;
	char send_buf[1024];

	send(ssock, "<tr>", strlen("<tr>"), 0);
	for (i = 0; i<SERVER_NUM; i++){
		if (strlen(sc[i].host) != 0)
		{
			sprintf(send_buf, "<td>%s</td>", sc[i].host);
			send(ssock, send_buf, strlen(send_buf), 0);
			sc[i].id = conn;
			conn++;
		}
		else{
			sprintf(send_buf, "<td>No user</td>");
			send(ssock, send_buf, strlen(send_buf), 0);
		}
	}
	send(ssock, "</tr>", strlen("</tr>"),0);
	send(ssock, "<tr>", strlen("<tr>"),0);
	for (i = 0; i<SERVER_NUM; i++){
			sprintf(send_buf, "<td valign=\"top\" id=\"m%d\"></td>", i);
			send(ssock, send_buf, strlen(send_buf), 0);
	}
	send(ssock, "</tr>\n", strlen("</tr>\n"), 0);
	send(ssock, "</table>\n", strlen("</table>\n"), 0);

}
void end_html(SOCKET ssock){
	send(ssock, "</body>\n", strlen("</body>\n"), 0);
	send(ssock, "</html>\n", strlen("</html>\n"), 0);
}
void begin_html(SOCKET ssock)
{
	send(ssock, "<html>"
		"<head>"
		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />"
		"</head>"
		"<body bgcolor=#336699>"
		"<font face=\"Courier New\" size=2 color=#FFFF99>"
		"<table width=\"800\" border=\"1\">",
		strlen("<html>"
		"<head>"
		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />"
		"</head>"
		"<body bgcolor=#336699>"
		"<font face=\"Courier New\" size=2 color=#FFFF99>"
		"<table width=\"800\" border=\"1\">"), 0);
}
int readline(FILE *fd, char *ptr, int maxlen)
{
	int n, rc;
	char c;
	*ptr = 0;
	for (n = 1; n < maxlen; n++)
	{
		if ((c = fgetc(fd)) != EOF)
		{
			*ptr++ = (char)c;
			if (c == '\n')	break;
		}
		else if (c == EOF)
		{
			if (n == 1)	return 0;
			else	break;
		}
		else
			return -1;
	}
	return n;
}
int EditPrintf(HWND hwndEdit, TCHAR * szFormat, ...)
{
	TCHAR   szBuffer[1024];
	va_list pArgList;

	va_start(pArgList, szFormat);
	wvsprintf(szBuffer, szFormat, pArgList);
	va_end(pArgList);

	SendMessage(hwndEdit, EM_SETSEL, (WPARAM)-1, (LPARAM)-1);
	SendMessage(hwndEdit, EM_REPLACESEL, FALSE, (LPARAM)szBuffer);
	SendMessage(hwndEdit, EM_SCROLLCARET, 0, 0);
	return SendMessage(hwndEdit, EM_GETLINECOUNT, 0, 0);
}

