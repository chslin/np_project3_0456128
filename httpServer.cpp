#include <limits.h>
#include <errno.h>
#include <ctype.h>
#include <fcntl.h>
#include <netdb.h>
#include <dirent.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>

#include <sys/param.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>

#include <getopt.h>
using namespace std;
/*
	HTTPd:hw3_0456128/1.0.0_alpha
	version :final
*/
#define BUFFER_SIZE				8888
#define REQUEST_MAX_SIZE		12340

#define Envnum					12
#define MaxLine 				1024

struct st_request_info {
	char *method;
	char *pathinfo;
	char *query;
	char *protocal;
	char *path;
	char *file;
	char *physical_path;
	char *version;
	char *request;
	char *name;
};

void printerror(char *err_Msg){
    perror(err_Msg); 
    exit(1); 
}

static int file_exists(const char *filename){
    struct stat buf;
    if (stat(filename, &buf) < 0){
        if (errno == ENOENT){
            return 0;
        }
    }
	
    return 1;
}



static char *substr( const char *s, int start_pos, int length, char *ret ){
    char buf[length+1];
    int i, j, end_pos;
	int str_len = strlen(s);

    if (str_len <= 0 || length < 0){
		return "";        
	}
	if (length == 0){
		length = str_len - start_pos;
	}
	start_pos = ( start_pos < 0 ? (str_len + start_pos) : ( start_pos==0 ? start_pos : start_pos-- ) );
	end_pos = start_pos + length;

    for(i=start_pos, j=0; i<end_pos && j<=length; i++, j++){
		buf[j] = s[i];        
	}
    buf[length] = '\0';
    strcpy(ret, buf);

    return(ret);
}



static void explode(char *from, char delim, char ***to, int *item_num){
    int i, j, k, n, temp_len;
    int max_len = strlen(from) + 1;
    char buf[max_len], **ret;
       
    for(i=0, n=1; from[i]!='\0'; i++){
        if (from[i] == delim) n++;
    }
    
    ret = (char **)malloc(n*sizeof(char *));
    for (i=0, k=0; k<n; k++){
        memset(buf, 0, max_len);     
        for(j=0; from[i]!='\0' && from[i]!=delim; i++, j++) buf[j] = from[i];
        i++;
        temp_len = strlen(buf)+1;
        ret[k] = (char*)malloc(temp_len);
        memcpy(ret[k], buf, temp_len);
    } 
    *to = ret;
    *item_num = n;
}

static int SendError(int client_sock, int status, char *title, char *extra_header, char *text ){
    char buf[BUFFER_SIZE], erromess[REQUEST_MAX_SIZE]={0};

    memset(buf, 0, strlen(buf));
    sprintf(buf, "%s\n", text);
    strcat(erromess, buf);
	
    write(client_sock, erromess, strlen(erromess));
	exit(0);
}
static char *strtolower( char *s ){
	int i, len = sizeof(s);
	for( i = 0; i < len; i++ ){
		s[i] = ( s[i] >= 'A' && s[i] <= 'Z' ? s[i] + 'a' - 'A' : s[i] );
	}
	return(s);
}


static int is_file(const char *filename){
	struct stat buf;
	if ( stat(filename, &buf) < 0 ){
		return -1;
	}
	if (S_ISREG(buf.st_mode)){
		return 1;
	}
	return 0;
}

static int is_dir(const char *filename){
	struct stat buf;
	if ( stat(filename, &buf) < 0 ){
		return -1;
	}
	if (S_ISDIR(buf.st_mode)){
		return 1;
	}
	return 0;
}

static long filesize(const char *filename){
    struct stat buf;
    if (!stat(filename, &buf)){
        return buf.st_size;
    }
    return 0;
}

static int strrpos (const char *s, char c){
	int i, len;
	if (!s || !c) return -1;
	len = strlen(s);
	for (i=len; i>=0; i--){
		if (s[i] == c) return i;
	}
	return -1;
}



void CGI(int sockfd, char *CGIName, char *Querry_string,struct st_request_info request_info){

	char *server_protocol = request_info.protocal;
	char *request_method = request_info.method;
	char *script_name = CGIName;
	char *remote_host = "npbsd1.cs.nctu.edu.tw";
	char *remote_addr = "140.113.235.148";
	char *auth_type = "everybody";
	char *remote_user = "helloworld";
	char *remote_ident = "1181";
	char *server_addr = "0.0.0.0";
	char events[Envnum][BUFFER_SIZE];


	char *argv[2];
	char *envp[Envnum];
	
	
	argv[0] = CGIName;
	argv[1] = 0;
	
	char path[BUFFER_SIZE];
	sprintf(path,"%s",CGIName);
	
	sprintf(events[0],"QUERY_STRING=%s",Querry_string);
	sprintf(events[1],"CONTENT_LENGTH=%d",strlen(Querry_string));
	sprintf(events[2],"REQUEST_METHOD=%s",request_method);
	sprintf(events[3],"SCRIPT_NAME=%s",script_name);
	sprintf(events[4],"REMOTE_HOST=%s",remote_host);
	sprintf(events[5],"REMOTE_ADDR=%s",remote_addr);
	sprintf(events[6],"AUTH_TYPE=%s",auth_type);
	sprintf(events[7],"REMOTE_USER=%s",remote_user);
	sprintf(events[8],"REMOTE_IDENT=%s",remote_ident);
	sprintf(events[9],"SERVER_PROTOCOL=%s",server_protocol);
	sprintf(events[10],"SERVER_ADDR=%s",server_addr);
	
	strcpy(events[11],"");
	/**/
	int i;
	for(i=0;i<Envnum;i++){
		envp[i] = events[i];
	}
	write(sockfd, "HTTP/1.1 200 OK\n", strlen("HTTP/1.1 200 OK\n"));
	dup2(sockfd,STDERR_FILENO);
	dup2(sockfd,STDOUT_FILENO);
	execve(path,argv,envp);
}

static int SendFile( int client_sock, char *filename, char *pathinfo ){
    char buf[128], contents[8192], mime_type[64];
    int fd;
	size_t file_size;


    if ( (fd = open(filename, O_RDONLY)) == -1 ){
        memset(buf, '\0', sizeof(buf));
        sprintf(buf, "Something unexpected went wrong read file %s.", pathinfo);
        SendError( client_sock, 500, "Internal Server Error", "", buf);
    }

	
	file_size = filesize(filename);
	if ( file_size < 8192){
		read(fd, contents, 8192);
		write( client_sock, contents, file_size );

	} else {
		while ( read(fd, contents, 8192) > 0 ){
			write( client_sock, contents, 8192 );
			memset(contents, '\0', sizeof(contents));
		}
	}

    close( fd );

	return 0;
}

static int ProcRequest( int client_sock, struct sockaddr_in client_addr, struct st_request_info request_info ){
	char buf[128] ={0};

	if ( !file_exists( request_info.physical_path ) ){
		sprintf(buf, "File %s not found.", request_info.pathinfo);
		SendError( client_sock, 404, "Not Found", "", buf);
	}
	if ( access(request_info.physical_path, R_OK) != 0 ){
		sprintf(buf, "File %s is protected.", request_info.pathinfo);
		SendError( client_sock, 403, "Forbidden", "", buf);
	}

	if ( is_file(request_info.physical_path) == 1 ){
		char * dot; 
	
		dot = strrchr(request_info.physical_path, '.');
		if(strcmp( dot, ".cgi" ) == 0 ){
			CGI(client_sock,request_info.physical_path,request_info.query,request_info );
			printf("");
		}else{
			SendFile( client_sock,  request_info.physical_path, request_info.pathinfo );
		}
	
	} else {
		sprintf(buf, "File %s is protected.", request_info.pathinfo);
		SendError( client_sock, 403, "Forbidden", "", buf);		
	}

	return 0;
}



static int ParseRequest( int client_sock, struct sockaddr_in client_addr, char *req ){
	char **buf, **method_buf, **query_buf,cwd[MaxLine], tmp_path[1536], pathinfo[512], path[256], file[256], log[MaxLine];
	int line_total, method_total, query_total, i;
	struct st_request_info request_info;

	explode(req, '\n', &buf, &line_total);

	if (strcmp(buf[0], "\n") == 0 || strcmp(buf[0], "\r\n") == 0){
		SendError( client_sock, 400, "Bad Request", "", "Can't parse request." );
	}

	explode(buf[0], ' ', &method_buf, &method_total);
	if ( strcmp( strtolower(method_buf[0]), "get") != 0 &&  strcmp( strtolower(method_buf[0]), "head") != 0 ){
		SendError( client_sock, 501, "Not Implemented", "", "That method is not implemented." );
	}
	
	explode(method_buf[1], '?', &query_buf, &query_total);

	getcwd(cwd, sizeof(cwd));
	strcpy(pathinfo, query_buf[0]);
	substr(query_buf[0], 0, strrpos(pathinfo, '/')+1, path);
	substr(query_buf[0], strrpos(pathinfo, '/')+1, 0, file);
	
	memset(&request_info, 0, sizeof(request_info));
	strcat(cwd, pathinfo);

	request_info.method			= method_buf[0];
	request_info.pathinfo		= pathinfo;
	if(query_total == 2){
		request_info.query	= query_buf[1];
	}else{
		 request_info.query	= "";
	}
	
	request_info.protocal		= strtolower(method_buf[2]);
	request_info.path			= path;
	request_info.file			= file;
	request_info.physical_path	= cwd;

	memset(tmp_path, 0, sizeof(tmp_path));
	strcpy(tmp_path, cwd);
	if ( is_dir(tmp_path) ){
		strcat(tmp_path, "DEFAULT_DIRECTORY_INDEX");
		if ( file_exists(tmp_path) ){
			request_info.physical_path = tmp_path;
		}
	}

	ProcRequest( client_sock, client_addr, request_info );

	return 0;
}




static void HandleClient( int client_sock, struct sockaddr_in client_addr ){
    char buf[REQUEST_MAX_SIZE];
	char GET[REQUEST_MAX_SIZE];
	if ( read(client_sock, buf, REQUEST_MAX_SIZE) < 0){
		SendError( client_sock, 500, "Internal Server Error", "", "Client request not success." );
		printerror("read error");
	}
	
	ParseRequest( client_sock, client_addr, buf );

    close(client_sock);
    exit(0);
}



static void InitServerListen( unsigned int port, unsigned int max_client ){
    int serversock, clientsock;
    struct sockaddr_in server_addr, client_addr;
    /* Create the TCP socket */
    if ((serversock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0){
        printerror("Failed to create socket");
    }
    memset(&server_addr, 0, sizeof(server_addr));       
    server_addr.sin_family = AF_INET;                  
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);   
    server_addr.sin_port = htons(port);          
    if (bind(serversock, (struct sockaddr *) &server_addr, sizeof(server_addr)) < 0){
        printerror("bind erro");
    }
    if (listen(serversock, max_client) < 0){
        printerror("listen erro");
    }
	printf("Start server listening at port %d ...\n", port);
    while (true){
        unsigned int clientlen = sizeof(client_addr);
        if ((clientsock = accept(serversock, (struct sockaddr *) &client_addr, &clientlen)) < 0){
            printerror("acceptted erro");
        }
        if ( fork() == 0 ){
			HandleClient(clientsock, client_addr);
		} else {
			wait(NULL); 
		}
        close(clientsock);
    }

}
int main( int argc, char *argv[] ){
	//chdir("/net/gcs/104/0456128/public_html");
	InitServerListen( atoi(argv[1]), 5 );
}